﻿/**
* BondAlgoExecutionService.hpp
* Defines the data types and Service for executions.
*
* @author Corbin Guan
*/
#ifndef BOND_ALGO_EXECUTION_SERVICE_HPP
#define BOND_ALGO_EXECUTION_SERVICE_HPP

#include <fstream>
#include "executionservice.hpp"
#include "products.hpp"

// Define ALgoExecution class as an object with reference to the ExecutionOrder class
//template <typename T>
//class AlgoExecution 
//{
//public:
//	AlgoExecution(ExecutionOrder<T>& EO);
//	ExecutionOrder<T> GetExecutionOrder();
//
//private:
//	ExecutionOrder<T> EO;
//};
//
//template <typename T>
//AlgoExecution<T>::AlgoExecution(ExecutionOrder<T>& newEO)
//{
//	EO.UpdateExecutionOrder(newEO);
//}
//
//template <typename T>
//ExecutionOrder<T> AlgoExecution<T>::GetExecutionOrder()
//{
//	return EO;
//}

class BondAlgoExecutionService : public Service<string, ExecutionOrder<Bond> >
{
public:
	// Default ctor
	BondAlgoExecutionService();

	// keyed on product Id, execute market buy and sell order alternatively
	void ExecuteOrder(OrderBook<Bond> &data);

	// Get data on our service given a key
	ExecutionOrder<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(ExecutionOrder<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<ExecutionOrder<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<ExecutionOrder<Bond> >* >& GetListeners() const;


private:
	multimap<string, ExecutionOrder<Bond> > AlgoExecutionMap;
	vector<ServiceListener<ExecutionOrder<Bond> >* > listenerVec;
};

#endif