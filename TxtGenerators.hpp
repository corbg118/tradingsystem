/*
TxtGenerator.hpp

This file is used to generate the following txt files:
trades.csv
price.csv
marketdata.csv
inquiries.csv

Author: Corbin Guan
*/

#ifndef TXTGENERATORS_HPP
#define TXTGENERATORS_HPP

#include <fstream>
#include <string>
#include <sstream>
#include <random>
#include <vector>
#include "products.hpp"
using namespace std;
map<string, Bond> bonds;

// transform the generated random number into a price string
string StringTypePrice(int num) {
	int num1 = num / 256, num2 = num % 256,
		num3 = num2 / 8, num4 = num2 % 8;
	string str1 = std::to_string(99 + num1) + "-",
		str2 = std::to_string(num3), str3 = std::to_string(num4);
	if (num3 < 10) str2 = "0" + str2;
	if (num4 == 4)	str3 = "+";
	return str1 + str2 + str3;
}

// I obtained some recent CUSIPs for each type of bond from the following website:
// https://www.treasurydirect.gov/instit/instit.htm?upcoming

vector<string> CUSIPs{
	"912828U99", // 2Y
	"912828U73", // 3Y
	"912828U81", // 5Y
	"912828U57", // 7Y
	"912828U24", // 10Y
	"912810RU4" // 30Y
};

void Trade_Generator() {
	ofstream os("trades.csv");
	os << "CUSIP|TradeID|BookID|Quantity|Side" << endl;

	for (int i = 0; i < 6; i++) {
		string CUSIP = CUSIPs[i];
		for (int j = 1; j <= 10; j++) {
			os << CUSIP << "|Trade#" << (i - 1) * 10 + j << "|TRSY" << 1 + rand() % 3 << "|" << (1 + rand() % 9) * 1000000 << "|" << (rand() % 2 == 1 ? "BUY" : "SELL") << endl;
		}
	}
}

void MD_Generator() {
	ofstream os("marketdata.csv");
	os << "CUSIP|BP1|Qty1|BP2|Qty2|BP3|Qty3|BP4|Qty4|BP5|Qty5|OP1|Qty1|OP2|Qty2|OP3|Qty3|OP4|Qty4|OP5|Qty5" << endl;
	// string price, quantity, side;
	for (int j = 0; j < 10; j++) {
		for (int i = 1; i <= 6; i++) {
			string CUSIP = CUSIPs[i - 1];
			os << CUSIP << '|';
			int mid_num = rand() % (256 * 2 + 1);

			// output bid prices in descending order
			int bid_num = mid_num - 1;
			for (int k = 1; k <= 5; k++) {
				string bid_price = StringTypePrice(bid_num);
				int quantity = 1000000 * k;
				os << bid_price << '|' << quantity << '|';
				bid_num--;
			}
			// output all offer prices in ascending order
			int offer_num = mid_num + 1;
			for (int k = 1; k <= 5; k++) {
				string offer_price = StringTypePrice(offer_num);
				int quantity = 1000000 * k;
				os << offer_price << '|' << quantity << '|';
				offer_num++;
			}
			os << endl;
		}
	}
}

void Price_Generator() {
	ofstream os("prices.csv");
	os << "CUSIP|MidPrice|BidAskSpread" << endl;

	for (int count = 0; count < 100; ++count) {
		for (int i = 0; i < 6; ++i) {
			string CUSIP = CUSIPs[i];
			os << CUSIP << "|";

			int _MidPrice = rand() % (256 * 2 - 8) + 4;
			string _MidPriceStr = StringTypePrice(_MidPrice);

			int _BidAskSpread = (rand() % 3 + 2);
			string _BidAskSpreadStr = "0-00" + (_BidAskSpread == 4 ? "+" : std::to_string(_BidAskSpread));
			os << _MidPriceStr << '|' << _BidAskSpreadStr << endl;
		}
	}
}

void Inquiries_Generator() {
	ofstream os("inquiries.csv");
	os << "InquiryId|CUSIP|Side|Quantity|State" << endl;
	string InquiryId, CUSIP, _Side, _Qty, _State;
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 6; j++) {
			InquiryId = "Inquiry#" + std::to_string(j * 10 + i);
			CUSIP = CUSIPs[j];
			if (rand() % 2 == 0) _Side = "BUY";
			else _Side = "SELL";
			_Qty = std::to_string(rand() * 1000);
			_State = "RECEIVED";
			os << InquiryId + '|' + CUSIP + '|' + _Side + '|' + _Qty + '|'+ _State << endl;
		}
	}
}

#endif