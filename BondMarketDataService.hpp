/**
* BondMarketDataService.hpp
* Defines the data types and Service for market data.
*
* @author Corbin Guan
*/
#ifndef BONDMARKETDATASERVICE_HPP
#define BONDMARKETDATASERVICE_HPP

#include <string>
#include <vector>
#include <map>
#include "marketdataservice.hpp"
#include "products.hpp"

/**
* Market Data Service which distributes market data
* Keyed on product identifier.
* Type T is the product type.
*/
class BondMarketDataService : public MarketDataService <Bond>
{
private:
	multimap<string, OrderBook<Bond> > order_map; // Construct a multimap here because there are multiple orders related to one CUSIP
	vector < ServiceListener<OrderBook<Bond> > * > listenerVec; // this vector is used to store the Listesners

public:
	// default ctor
	BondMarketDataService();

	// Add order to the order_map when called in OnMessage
	void AddOrder(OrderBook<Bond> &order);

	// Get the best bid/offer order
	const BidOffer GetBestBidOffer(const string &productId);

	// Aggregate the order book
	const OrderBook<Bond>& AggregateDepth(const string &productId);

	// Get data on our service given a key
	OrderBook<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(OrderBook<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<OrderBook<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<OrderBook<Bond> >* >& GetListeners() const;

};

#endif