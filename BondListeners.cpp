/**
* BondListeners.cpp
* Defines various kinds of service liteners used in the platform.
*
* @author Corbin Guan
*/
#include <iostream>
#include "BondListeners.hpp"
#include "soa.hpp"

using namespace std;

// BTBS --> BPS
BTBSListener::BTBSListener(BondPositionService* BPS) : BPS(BPS) {};

void BTBSListener::ProcessAdd(Trade<Bond> &data) {
	cout << "The system is now transfering data from BTBS to BPS..." << endl;

	// Call the addTrade function inside BPS
	BPS->AddTrade(data);
}

void BTBSListener::ProcessRemove(Trade<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BTBSListener::ProcessUpdate(Trade<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary 
}

// BPS --> BRS
BPSListener::BPSListener(BondRiskService* BRS) : BRS(BRS) {};

void BPSListener::ProcessAdd(Position<Bond> &data) {
	cout << "The system is now transfering data from BPS to BRS..." << endl;

	// Call the AddPosition function inside BRS
	BRS->AddPosition(data);
}

void BPSListener::ProcessRemove(Position<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BPSListener::ProcessUpdate(Position<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BMDS --> BAES (Algo execution)
BondMDSListener_BAES::BondMDSListener_BAES(BondAlgoExecutionService* BAES) : BAES(BAES) {};

void BondMDSListener_BAES::ProcessAdd(OrderBook<Bond> &data) {
	cout << "The system is now transfering data from BMDS to BAES..." << endl;

	// Call the AddPosition function inside BAES
	BAES->ExecuteOrder(data);
}

void BondMDSListener_BAES::ProcessRemove(OrderBook<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BondMDSListener_BAES::ProcessUpdate(OrderBook<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BAES --> BES (Execution)
BondAESListener::BondAESListener(BondExecutionService* BES) : BES(BES) {};

void BondAESListener::ProcessAdd(ExecutionOrder<Bond> &data) {
	cout << "The system is now transfering data from BAES to BES..." << endl;

	// Call the AddPosition function inside BES
	BES->ExecuteOrder(data, CME);
}

void BondAESListener::ProcessRemove(ExecutionOrder<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BondAESListener::ProcessUpdate(ExecutionOrder<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BPRS (Pricing) --> BASS (Algo streaming)
BondPRSListener::BondPRSListener(BondAlgoStreamingService* BASS) : BASS(BASS) {};

void BondPRSListener::ProcessAdd(Price<Bond> &data) {
	cout << "The system is now transfering data from BPRS to BASS..." << endl;

	double _bidPrice = data.GetMid() - data.GetBidOfferSpread() / 2;
	double _askPrice = data.GetMid() + data.GetBidOfferSpread() / 2;

	PriceStreamOrder _bidOrder(_bidPrice, 100000, 0, BID);
	PriceStreamOrder _askOrder(_askPrice, 100000, 0, OFFER);

	PriceStream<Bond> ps(data.GetProduct(), _bidOrder, _askOrder);

	// Call the AddPosition function inside BASS
	BASS->PublishPrice(ps);
}

void BondPRSListener::ProcessRemove(Price<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BondPRSListener::ProcessUpdate(Price<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BASS --> BSS (Streaming)
BondASSListener::BondASSListener(BondStreamingService* BSS) : BSS(BSS) {};

void BondASSListener::ProcessAdd(PriceStream<Bond> &data) {
	cout << "The system is now transfering data from BASS to BSS..." << endl;

	// Call the AddPosition function inside BSS
	BSS->PublishPrice(data);
}

void BondASSListener::ProcessRemove(PriceStream<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BondASSListener::ProcessUpdate(PriceStream<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BPS (position) --> BHDS
BondPSListener_BHDS::BondPSListener_BHDS(BondHistoricalDataService_BPS* BHDS) : BHDS(BHDS) {};

void BondPSListener_BHDS::ProcessAdd(Position<Bond> &data) {
	cout << "The system is now transfering data from BPS to BHDS..." << endl;
	
	// Set up a persisKey, add in 3 components to make sure the uniqueness of the key
	string _persistKey = "PersisId#" + data.GetProduct().GetProductId() + "_BPS_BHDS_" + std::to_string(rand() * 1000000);

	// Call the PersistData function inside BHDS
	BHDS->PersistData(_persistKey, data);
}

void BondPSListener_BHDS::ProcessRemove(Position<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BondPSListener_BHDS::ProcessUpdate(Position<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BRS --> BHDS
BondRSListener_BHDS::BondRSListener_BHDS(BondHistoricalDataService_BRS* BHDS) : BHDS(BHDS) {};

void BondRSListener_BHDS::ProcessAdd(PV01<Bond> &data) {
	cout << "The system is now transfering data from BRS to BHDS..." << endl;
	
	// Set up a persisKey, add in 3 components to make sure the uniqueness of the key
	string _persistKey = "PersisId#" + data.GetProducts().at(0).GetProductId() + "_BRS_BHDS_" + std::to_string(rand() * 1000000);

	// Call the PersistData function inside BHDS
	BHDS->PersistData(_persistKey, data);
}

void BondRSListener_BHDS::ProcessRemove(PV01<Bond>  &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BondRSListener_BHDS::ProcessUpdate(PV01<Bond>  &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BES --> BHDS
BondESListener_BHDS::BondESListener_BHDS(BondHistoricalDataService_BES* BHDS) : BHDS(BHDS) {};

void BondESListener_BHDS::ProcessAdd(ExecutionOrder<Bond> &data) {
	cout << "The system is now transfering data from BES to BHDS..." << endl;
	
	// Set up a persisKey, add in 3 components to make sure the uniqueness of the key
	string _persistKey = "PersisId#" + data.GetProduct().GetProductId() + "_BES_BHDS_" + std::to_string(rand() * 1000000);

	// Call the PersistData function inside BHDS
	BHDS->PersistData(_persistKey, data);
}

void BondESListener_BHDS::ProcessRemove(ExecutionOrder<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BondESListener_BHDS::ProcessUpdate(ExecutionOrder<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BSS --> BHDS
BondSSListener_BHDS::BondSSListener_BHDS(BondHistoricalDataService_BSS* BHDS) : BHDS(BHDS) {};

void BondSSListener_BHDS::ProcessAdd(PriceStream<Bond> &data) {
	cout << "The system is now transfering data from BSS to BHDS..." << endl;
	
	// Set up a persisKey, add in 3 components to make sure the uniqueness of the key
	string _persistKey = "PersisId#" + data.GetProduct().GetProductId() + "_BSS_BHDS_" + std::to_string(rand() * 1000000);

	// Call the PersistData function inside BHDS
	BHDS->PersistData(_persistKey, data);
}

void BondSSListener_BHDS::ProcessRemove(PriceStream<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

void BondSSListener_BHDS::ProcessUpdate(PriceStream<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}

// BIS --> BHDS
BondISListener_BHDS::BondISListener_BHDS(BondHistoricalDataService_BIS* BHDS): BHDS(BHDS) {}

void BondISListener_BHDS::ProcessAdd(Inquiry<Bond> &data) {
	cout << "The system is now transfering data from BIS to BHDS..." << endl;
	
	// Set up a persisKey, add in 3 components to make sure the uniqueness of the key
	string _persistKey = "PersisId#" + data.GetProduct().GetProductId() + "_BIS_BHDS_" + std::to_string(rand() * 1000000);

	// Call the PersistData function inside BHDS
	BHDS->PersistData(_persistKey, data);
}

void BondISListener_BHDS::ProcessRemove(Inquiry<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary 
}

void BondISListener_BHDS::ProcessUpdate(Inquiry<Bond> &data)
{
	// Intentionally left as empty, this function is not necessary
}