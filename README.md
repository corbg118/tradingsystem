## MTH9815 Final Project ##
### Author: CorbinGuan ###

### Running the code: ###
To run the code using g++, please run the following makefile:

```
make
./output
```

I've initialized and called necessary connectors /functions in the Main.cpp.


###The flow for the service classes is as follows:###

1. trades.csv -> *BondTradeBookingServiceConnector* -> **BondTradeBookingService** -> *BondTBSListener* -> **BondPositionService** -> *BPSListener* -> **BondRiskService** -> *BRSListener* -> **BondHistoricalDataService** -> *BHDSConnector* -> risk.csv

2. prices.csv -> *BondPricingServiceConnector* -> **BondPricingService** -> *BondPRSListener* -> **BondAlgoStreamingService** -> *BASSListener* -> **BondStreamingService** -> *BSSConnector* -> BondStreaming.csv

3. **BondStreamingService** -> *BSSListener* -> **BondHistoricalDataService** -> *BHDSConnector* -> streaming.csv

4. marketdata.csv -> *BondMarketDataServiceConnector* -> **BondMarketDataService** -> *BondMDSListener* -> **BondAlgoExecutionService** -> *BAESListener* -> **BondExecutionService** -> *BESConnector* -> BondExecution.csv

5. **BondExecutionService** -> *BESListener* -> **BondHistoricalDataService** -> *BHDSConnector* -> executions.csv

6. inquiries.csv -> *BondInquiryServiceConnector* -> **BondInquiryService** -> *BISListener* -> **BondHistoricalDataService** -> *BHDSConnector* -> allinquiries.csv

### More about the files ###
* All listeners mentioned above are defined under BondLinsteners.hpp and BondLinsteners.cpp 
* All connectors mentioned above are defined under BondConnectors.hpp and BondConnectors.cpp

### Minor FollowUp (DONE) ### 
I will upload the readily-available marketdata.csv, trades.csv, prices.csv and inquiries.csv files on to bitbucket as soon as possible. In that case, the file generation functions called in the Main.cpp can be excluded.