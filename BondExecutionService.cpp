/**
* BondExecutionService.cpp
* Defines the data types and Service for executions.
*
* @author Corbin Guan
*/

#include "BondExecutionService.hpp"
#include "BondListeners.hpp"
#include <fstream>

// default ctor
BondExecutionService::BondExecutionService(BESConnector* BESC) : BESC(BESC) {}

// keyed on product Id, execute market buy and sell order alternatively
void BondExecutionService::ExecuteOrder(ExecutionOrder<Bond>& order, Market market)
{
	BESC->Publish(order); 
	for (auto listener : listenerVec)
	{
		listener->ProcessAdd(order);
	}
}

// Get data on our service given a key
ExecutionOrder<Bond>& BondExecutionService::GetData(string key)
{
	// Return the latest orderbook encountered
	auto it = ExecutionMap.upper_bound(key);
	return it->second;
}

// The callback that a Connector should invoke for any new or updated data
void BondExecutionService::OnMessage(ExecutionOrder<Bond> &data)
{}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondExecutionService::AddListener(ServiceListener<ExecutionOrder<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<ExecutionOrder<Bond> >* >& BondExecutionService::GetListeners() const
{
	return listenerVec;
}