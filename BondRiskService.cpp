/**
* BondRiskService.cpp
* Defines the data types and Service for risk.
*
* @author Corbin Guan
*/
#include <vector>
#include <map>
#include "BondRiskService.hpp"
#include "BondListeners.hpp"

extern map<string, double> PV01_map;

BondRiskService::BondRiskService() : RiskService<Bond>() {}

// Add a position that the service will risk
void BondRiskService::AddPosition(Position<Bond> &position)
{
	// call the updateMap function to input PV01 data inside the map
	cout << "The system is now updating the risk..." << endl;

	vector<Bond> _prod;
	_prod.push_back(position.GetProduct());
	string CUSIP = position.GetProduct().GetProductId();
	long _Qty = position.GetAggregatePosition();
	double _PV01 = PV01_map[CUSIP] * position.GetAggregatePosition();
	PV01<Bond> pv01(_prod, _PV01, _Qty);

	if (risk.find(CUSIP) == risk.end()) // there is no such CUSIP so far, so we need to add a CUSIP
		risk.insert(std::pair<string, PV01<Bond> >(CUSIP, pv01));
	// Update the risk
	else
		risk.at(CUSIP).UpdatePV01(_PV01, _Qty);

	for (auto listener : listenerVec)
		listener->ProcessAdd(risk.at(CUSIP));

	//cout << "CUSIP: " << CUSIP << " PV01: " << risk.at(CUSIP).GetPV01() << " Qty: " << risk.at(CUSIP).GetQuantity() << endl;
}

// Get the bucketed risk for the bucket sector
const PV01<Bond> BondRiskService::GetBucketedRisk(const BucketedSector<Bond> &sector) const
{
	vector<Bond> _prod = sector.GetProducts();
	double _PV01 = 0;
	long _Qty = 0;
	// Aggregate the PV01 of all products inside sector
	for (auto prod : _prod)
	{
		string CUSIP = prod.GetProductId();
		_PV01 += risk.at(CUSIP).GetPV01();
		_Qty += risk.at(CUSIP).GetQuantity();
	}

	//PV01<Bond> agg_pv01(_prod, _PV01, _Qty);

	return PV01<Bond>(_prod, _PV01, _Qty);
}

// Get data on our service given a key
PV01<Bond>& BondRiskService::GetData(string key)
{
	return risk[key]; // return the position corresponding to the key
}

// The callback that a Connector should invoke for any new or updated data
void BondRiskService::OnMessage(PV01<Bond> &data)
{
	// Intentionally left blank as no connector is linked to this service
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondRiskService::AddListener(ServiceListener<PV01<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<PV01<Bond> >* >& BondRiskService::GetListeners() const
{
	return listenerVec;
}