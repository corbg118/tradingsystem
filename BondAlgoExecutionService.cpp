/**
* BondAlgoExecutionService.cpp
* Defines the data types and Service for executions.
*
* @author Corbin Guan
*/

#include "BondAlgoExecutionService.hpp"
#include "BondListeners.hpp"

// default ctor
BondAlgoExecutionService::BondAlgoExecutionService() {}

// keyed on product Id, execute market buy and sell order alternatively
void BondAlgoExecutionService::ExecuteOrder(OrderBook<Bond>& data)
{
	double BidPrice = data.GetBidStack().at(0).GetPrice(); // This returns the best bid price of the order
	double BidQty = data.GetBidStack().at(0).GetQuantity(); // This returns the best bid price of the order, here we assume all are visible
	ExecutionOrder<Bond> AEBid(data.GetProduct(), BID, "Order#", MARKET, BidPrice, BidQty, 0, "OrderID", 0);

	double AskPrice = data.GetOfferStack().at(0).GetPrice(); // This returns the best bid price of the order
	double AskQty = data.GetOfferStack().at(0).GetQuantity(); // This returns the best bid price of the order, here we assume all are visible
	ExecutionOrder<Bond> AEAsk(data.GetProduct(), OFFER, "Order#", MARKET, AskPrice, AskQty, 0, "OrderID", 0);
	
	AlgoExecutionMap.insert(std::pair<string, ExecutionOrder<Bond> >(data.GetProduct().GetProductId(), AEBid));
	AlgoExecutionMap.insert(std::pair<string, ExecutionOrder<Bond> >(data.GetProduct().GetProductId(), AEAsk));

	for ( auto listener : listenerVec)
	{
		listener->ProcessAdd(AEBid);
		listener->ProcessAdd(AEAsk);
	}

	cout << "Product: " << data.GetProduct().GetProductId() << "AEBid: " << AEBid.GetOrderId() << " " << AEBid.GetVisibleQuantity() << endl;
}

// Get data on our service given a key
ExecutionOrder<Bond>& BondAlgoExecutionService::GetData(string key)
{
	// Return the latest orderbook encountered
	auto it = AlgoExecutionMap.upper_bound(key);
	return it->second;
}

// The callback that a Connector should invoke for any new or updated data
void BondAlgoExecutionService::OnMessage(ExecutionOrder<Bond> &data)
{
	// To update 

}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondAlgoExecutionService::AddListener(ServiceListener<ExecutionOrder<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<ExecutionOrder<Bond> >* >& BondAlgoExecutionService::GetListeners() const
{
	return listenerVec;
}