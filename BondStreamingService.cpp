/**
* BondStreamingService.cpp
* Defines the data types and Service for Streaming.
*
* @author Corbin Guan
*/

#include "BondStreamingService.hpp"
#include "BondListeners.hpp"

// default ctor
BondStreamingService::BondStreamingService(BSSConnector* BSSC) : BSSC(BSSC) {}

// keyed on product Id, execute market buy and sell order alternatively
void BondStreamingService::PublishPrice(PriceStream<Bond>& order)
{
	BSSC->Publish(order);
	for (auto listener : listenerVec)
	{
		listener->ProcessAdd(order);
	}

}

// Get data on our service given a key
PriceStream<Bond>& BondStreamingService::GetData(string key)
{
	// Return the latest orderbook encountered
	auto it = StreamingMap.upper_bound(key);
	return it->second;
}

// The callback that a Connector should invoke for any new or updated data
void BondStreamingService::OnMessage(PriceStream<Bond> &data)
{}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondStreamingService::AddListener(ServiceListener<PriceStream<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<PriceStream<Bond> >* >& BondStreamingService::GetListeners() const
{
	return listenerVec;
}