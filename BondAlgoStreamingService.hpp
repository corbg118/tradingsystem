/**
* BondAlgoExecutionService.hpp
* Defines the data types and Service for executions.
*
* @author Corbin Guan
*/
#ifndef BONDALGOSTREAMINGSERVICE_HPP
#define BONDALGOSTREAMINGSERVICE_HPP

#include <fstream>
#include "streamingservice.hpp"
#include "products.hpp"

class BondAlgoStreamingService : public Service<string, PriceStream<Bond> >
{
public:
	// Default ctor
	BondAlgoStreamingService();

	// keyed on product Id, execute market buy and sell order alternatively
	void PublishPrice(PriceStream<Bond> &data);

	// Get data on our service given a key
	PriceStream<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(PriceStream<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<PriceStream<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<PriceStream<Bond> >* >& GetListeners() const;


private:
	multimap<string, PriceStream<Bond> > AlgoStreamingMap;
	vector<ServiceListener<PriceStream<Bond> >* > listenerVec;
};

#endif