/**
* bondpricingservice.hpp
* Defines the data types and Service for internal prices.
*
* @author Corbin Guan
*/
#ifndef BONDPRICINGSERVICE_HPP
#define BONDPRICINGSERVICE_HPP

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <map>
#include "soa.hpp"
#include "products.hpp"
#include "pricingservice.hpp"

/**
* Pricing Service managing mid prices and bid/offers.
* Keyed on product identifier.
* Type T is the product type.
*/
class BondPricingService : public PricingService<Bond>
{
public:
	BondPricingService();

	// Add price into the price_map
	void AddPrice(Price<Bond> &data);

	// Get data on our service given a key
	Price<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Price<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<Price<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<Price<Bond> >* >& GetListeners() const;


private:
	vector < ServiceListener<Price<Bond> > * > listenerVec; // this vector is used to store the Listesners
	multimap<string, Price<Bond> > price_map;
};


#endif
