/**
* bondhistoricaldataservice.hpp
* Defines the data types and Service for Bond historical data.
*
* @author Corbin Guan
*/

#ifndef BOND_HISTORICAL_DATA_SERVICE_HPP
#define BOND_HISTORICAL_DATA_SERVICE_HPP

#include "historicaldataservice.hpp"
#include "BondConnectors.hpp"
#include "products.hpp"
#include "soa.hpp"

/**
* Service for processing and persisting historical data to a persistent store.
* Keyed on some persistent key.
*/

class BondHistoricalDataService_BPS : HistoricalDataService <Position<Bond> >
{

private:
	map<string, Position<Bond> > history_map; // string is the persistkey
	vector<ServiceListener<Position<Bond> >* > listenerVec;
	BHDSConnector_BPS* BHDSC;

public:

	BondHistoricalDataService_BPS(BHDSConnector_BPS* BHDSC);

	// Persist data to a store
	void PersistData(string persistKey,  Position<Bond>& data);

	// Get data on our service given a key
	Position<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Position<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<Position<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<Position<Bond> >* >& GetListeners() const;

};

class BondHistoricalDataService_BRS : HistoricalDataService <PV01<Bond> >
{

private:
	map<string, PV01<Bond> > history_map; // string is the persistkey
	vector<ServiceListener<PV01<Bond> >* > listenerVec;
	BHDSConnector_BRS* BHDSC;

public:

	BondHistoricalDataService_BRS(BHDSConnector_BRS* BHDSC);

	// Persist data to a store
	void PersistData(string persistKey,  PV01<Bond>& data);

	// Get data on our service given a key
	PV01<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(PV01<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<PV01<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<PV01<Bond> >* >& GetListeners() const;

};

class BondHistoricalDataService_BES : HistoricalDataService <ExecutionOrder<Bond> >
{

private:
	map<string, ExecutionOrder<Bond> > history_map; // string is the persistkey
	vector<ServiceListener<ExecutionOrder<Bond> >* > listenerVec;
	BHDSConnector_BES* BHDSC;

public:

	BondHistoricalDataService_BES(BHDSConnector_BES* BHDSC);

	// Persist data to a store
	void PersistData(string persistKey, ExecutionOrder<Bond>& data);

	// Get data on our service given a key
	ExecutionOrder<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(ExecutionOrder<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<ExecutionOrder<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<ExecutionOrder<Bond> >* >& GetListeners() const;

};

class BondHistoricalDataService_BSS : HistoricalDataService <PriceStream<Bond> >
{

private:
	map<string, PriceStream<Bond> > history_map; // string is the persistkey
	vector<ServiceListener<PriceStream<Bond> >* > listenerVec;
	BHDSConnector_BSS* BHDSC;

public:

	BondHistoricalDataService_BSS(BHDSConnector_BSS* BHDSC);

	// Persist data to a store
	void PersistData(string persistKey, PriceStream<Bond>& data);

	// Get data on our service given a key
	PriceStream<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(PriceStream<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<PriceStream<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<PriceStream<Bond> >* >& GetListeners() const;

};

class BondHistoricalDataService_BIS : HistoricalDataService <Inquiry<Bond> >
{

private:
	map<string, Inquiry<Bond> > history_map; // string is the persistkey
	vector<ServiceListener<Inquiry<Bond> >* > listenerVec;
	BHDSConnector_BIS* BHDSC;

public:

	// ctor
	BondHistoricalDataService_BIS(BHDSConnector_BIS* _BHDSC);

	// Persist data to a store
	virtual void PersistData(string persistKey, Inquiry<Bond>& data);

	// Get data on our service given a key
	Inquiry<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Inquiry<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<Inquiry<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<Inquiry<Bond> >* >& GetListeners() const;

};

#endif
