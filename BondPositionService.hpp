/**
* BondPositionService.hpp
* Defines the data types and Service for trade booking.
*
* @author Corbin Guan
*/
#ifndef BONDPOSITIONSERVICE_HPP
#define BONDPOSITIONSERVICE_HPP

#include <string>
#include <vector>
#include <map>
#include "positionservice.hpp"
#include "products.hpp"

/**
* Position Service to manage positions across multiple books and secruties.
*/

class BondPositionService : public PositionService<Bond>
{
private:
	vector < ServiceListener<Position<Bond> > * > listenerVec; // this vector is used to store the Listesners
public:
	// Default ctor
	BondPositionService();

	map<string, Position<Bond> > pos; // <CUSIP, <TradeBook, Position> >

									 // Add a trade to the service
	void AddTrade(Trade<Bond> &trade);


	// Get data on our service given a key
	Position<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Position<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<Position<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<Position<Bond> >* >& GetListeners() const;
};

#endif