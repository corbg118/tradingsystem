/**
* BondAlgoStreamingService.cpp
* Defines the data types and Service for Streaming.
*
* @author Corbin Guan
*/

#include "BondAlgoStreamingService.hpp"
#include "BondListeners.hpp"

// default ctor
BondAlgoStreamingService::BondAlgoStreamingService()
{
	// ofstream os("BondStreaming.csv");
	// os << "CUSIP|Side|Price|VisibleQty|HiddenQty|Side|Price|VisibleQty|HiddenQty" << endl;
}

// keyed on product Id, execute market buy and sell order alternatively
void BondAlgoStreamingService::PublishPrice(PriceStream<Bond>& data)
{
	AlgoStreamingMap.insert(std::pair<string, PriceStream<Bond> >(data.GetProduct().GetProductId(), data));

	for (auto listener : listenerVec)
	{
		listener->ProcessAdd(data);
	}
}

// Get data on our service given a key
PriceStream<Bond>& BondAlgoStreamingService::GetData(string key)
{
	// Return the latest orderbook encountered
	auto it = AlgoStreamingMap.upper_bound(key);
	return it->second;
}

// The callback that a Connector should invoke for any new or updated data
void BondAlgoStreamingService::OnMessage(PriceStream<Bond> &data)
{
	// To update 

}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondAlgoStreamingService::AddListener(ServiceListener<PriceStream<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<PriceStream<Bond> >* >& BondAlgoStreamingService::GetListeners() const
{
	return listenerVec;
}