output: Main.o BondAlgoExecutionService.o BondAlgoStreamingService.o BondConnectors.o BondExecutionService.o BondHistoricalDataService.o BondInquiryService.o BondListeners.o BondMarketDataService.o BondPositionService.o BondPricingService.o BondRiskService.o BondStreamingService.o BondTradeBookingService.o marketdataservice.o products.o streamingservice.o
	g++ Main.o BondAlgoExecutionService.o BondAlgoStreamingService.o BondConnectors.o BondExecutionService.o BondHistoricalDataService.o BondInquiryService.o BondListeners.o BondMarketDataService.o BondPositionService.o BondPricingService.o BondRiskService.o BondStreamingService.o BondTradeBookingService.o marketdataservice.o products.o streamingservice.o -o output

Main.o: Main.cpp
	g++ -c Main.cpp

BondAlgoExecutionService.o: BondAlgoExecutionService.cpp BondAlgoExecutionService.hpp
	g++ -c BondAlgoExecutionService.cpp

BondAlgoStreamingService.o: BondAlgoStreamingService.cpp BondAlgoStreamingService.hpp
	g++ -c BondAlgoStreamingService.cpp

BondConnectors.o: BondConnectors.cpp BondConnectors.hpp
	g++ -c BondConnectors.cpp

BondExecutionService.o: BondExecutionService.cpp BondExecutionService.hpp
	g++ -c BondExecutionService.cpp

BondHistoricalDataService.o: BondHistoricalDataService.cpp BondHistoricalDataService.hpp
	g++ -c BondHistoricalDataService.cpp

BondInquiryService.o: BondInquiryService.cpp BondInquiryService.hpp
	g++ -c BondInquiryService.cpp

BondListeners.o: BondListeners.cpp BondListeners.hpp
	g++ -c BondListeners.cpp

BondMarketDataService.o: BondMarketDataService.cpp BondMarketDataService.hpp
	g++ -c BondMarketDataService.cpp

BondPositionService.o: BondPositionService.cpp BondPositionService.hpp
	g++ -c BondPositionService.cpp

BondPricingService.o: BondPricingService.cpp BondPricingService.hpp
	g++ -c BondPricingService.cpp

BondRiskService.o: BondRiskService.cpp BondRiskService.hpp
	g++ -c BondRiskService.cpp

BondStreamingService.o: BondStreamingService.cpp BondStreamingService.hpp
	g++ -c BondStreamingService.cpp

BondTradeBookingService.o: BondTradeBookingService.cpp BondTradeBookingService.hpp
	g++ -c BondTradeBookingService.cpp

marketdataservice.o: marketdataservice.cpp marketdataservice.hpp
	g++ -c marketdataservice.cpp

products.o: products.cpp products.hpp
	g++ -c products.cpp

streamingservice.o: streamingservice.cpp streamingservice.hpp
	g++ -c streamingservice.cpp

clean: 
	rm *.o output
