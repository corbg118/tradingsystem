/**
* BondInquiryService.cpp
* Defines the data types and Service for inquiries.
*
* @author Corbin Guan
*/

#include "BondInquiryService.hpp"
#include "BondListeners.hpp"

// Default ctor
BondInquiryService::BondInquiryService() : InquiryService() {}

// Get data on our service given a key
Inquiry<Bond>& BondInquiryService::GetData(string key)
{
	return inquiry_map[key]; // return the position corresponding to the key
}


// The callback that a Connector should invoke for any new or updated data
// Add Inquiry to map
void BondInquiryService::OnMessage(Inquiry<Bond> &data)
{
	// Add the inquiry to inquiry_map
	inquiry_map.insert(std::pair<string, Inquiry<Bond> >(data.GetInquiryId(), data));
	// we need to process the data here by using the following two functions, for demonstration purpose,
	// i use a random number to determine which function to call
	if (rand() % 2 == 0) SendQuote(data.GetInquiryId(), 101);
	else RejectInquiry(data.GetInquiryId());

	for (auto listener : listenerVec)
		listener->ProcessAdd(inquiry_map.at(data.GetInquiryId()));
	// At the same time, process the data and send to either SendQuote() or RejectInquiry()
}


// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondInquiryService::AddListener(ServiceListener<Inquiry<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<Inquiry<Bond> >* >& BondInquiryService::GetListeners() const
{
	return listenerVec;
}

// Send a quote back to the client
void BondInquiryService::SendQuote(const string &inquiryId, double price)
{
	// Update the price of the inquiry
	inquiry_map.at(inquiryId).UpdatePrice(price);
	InquiryState IS = QUOTED;
	inquiry_map.at(inquiryId).UpdateState(IS);
}

// Reject an inquiry from the client
void BondInquiryService::RejectInquiry(const string &inquiryId)
{
	InquiryState IS = REJECTED;
	inquiry_map.at(inquiryId).UpdateState(IS);
}

