/**
* BondPostionService.cpp
* Defines the data types and Service for position.
*
* @author Corbin Guan
*/

#include "BondPositionService.hpp"
#include "BondListeners.hpp"

// Default ctor
BondPositionService::BondPositionService() : PositionService() {}

// Add a trade to the service -- to figure out how to use this
void BondPositionService::AddTrade(Trade<Bond> &trade)
{	// add trade to the existing posion
	// Get the CUSID of the product traded
	cout << "The system is now booking trades into the Postion..." << endl;
	string CUSIP = trade.GetProduct().GetProductId();

	// Calculate the position size of the trade
	int PositionSize = 0;
	if (trade.GetSide() == BUY) // positive position size if BUY
		PositionSize = trade.GetQuantity();
	else // negative position size is SELL
		PositionSize = -trade.GetQuantity();
	string book = trade.GetBook();

	// Update the position of the BPS inside the Listener
	// pos is a map <CUSIP, <TradeBook, PositionSize> >
	// pos[CUSIP] is a position
	if (pos.find(CUSIP) == pos.end()) // there is no such CUSIP so far, so we need to add a CUSIP
	{
		Position<Bond> p(trade.GetProduct());
		pos.insert(std::pair<string, Position<Bond> >(CUSIP, p));
	}
	// Update the position
	pos[CUSIP].UpdatePosition(trade.GetBook(), PositionSize);

	for (auto listener : listenerVec)
		listener->ProcessAdd(pos[CUSIP]);

	//Test position function:
	cout << "CUSIP: " << CUSIP << " book: " << book << " size: "<<PositionSize<<" position: " << pos[CUSIP].GetPosition(book) << endl;
	cout << "Aggregate: " << pos[CUSIP].GetAggregatePosition() << endl;
}

// Get data on our service given a key
Position<Bond>& BondPositionService::GetData(string key)
{
	return pos[key]; // return the position corresponding to the key
}

// The callback that a Connector should invoke for any new or updated data
void BondPositionService::OnMessage(Position<Bond> &data)
{
	// Intentionally left blank as no connector is linked to this service
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondPositionService::AddListener(ServiceListener<Position<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<Position<Bond> >* >& BondPositionService::GetListeners() const
{
	return listenerVec;
}