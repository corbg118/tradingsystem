/**
* BondConnectors.hpp
* Defines various kinds of bond connectors
*
* @author Corbin Guan
*/

#ifndef BONDCONNECTORS_HPP
#define BONDCONNECTORS_HPP

#include <vector>
#include "soa.hpp"
#include "products.hpp"
#include "BondTradeBookingService.hpp"
#include "BondMarketDataService.hpp"
#include "BondPricingService.hpp"
#include "BondInquiryService.hpp"
#include "executionservice.hpp"
#include "streamingservice.hpp"
#include "positionservice.hpp"
#include "riskservice.hpp"

class BTBSConnector : public Connector<Trade<Bond> >
{
public:
	// Publish data to the Connector
	void Publish(Trade<Bond> &data);
	BTBSConnector(string, BondTradeBookingService*); // ctor

private:
	// BTBS is a private member of my BTBS service!!!
	BondTradeBookingService* BTBS;
};

class BMDSConnector : public Connector<OrderBook<Bond> >
{
public:
	// Publish data to the Connector
	void Publish(OrderBook<Bond> &data);
	BMDSConnector(string, BondMarketDataService*); // ctor

private:
	// BTBS is a private member of my BTBS service!!!
	BondMarketDataService* BMDS;
};

class BPRSConnector : public Connector<Price<Bond> >
{
public:
	// Publish data to the Connector
	void Publish(Price<Bond> &data);
	BPRSConnector(string, BondPricingService*);

private:
	// BTBS is a private member of my BTBS service!!!
	BondPricingService* BPRS;
};

class BESConnector : public Connector<ExecutionOrder<Bond> >
{
private:
	string path;

public:

	// default ctor
	BESConnector(string path);

	// Publish data to the Connector
	void Publish(ExecutionOrder<Bond> &data);

};

class BSSConnector : public Connector<PriceStream<Bond> >
{
private:
	string path;

public:
	// default ctor
	BSSConnector(string path);

	// Publish data to the Connector
	void Publish(PriceStream<Bond> &data);

};

class BISConnector : public Connector<Inquiry<Bond> >
{

public:

	// Publish data to the Connector
	void Publish(Inquiry<Bond> &data);
	BISConnector(string, BondInquiryService*);

private:
	BondInquiryService* BIS;
};

class BHDSConnector_BPS : public Connector<Position<Bond> >
{

public:

	// default ctor
	BHDSConnector_BPS(string path);

	// Publish data to the Connector
	void Publish(Position<Bond> &data);
	void Publish(string _PersistId, Position<Bond> &data);

private:
	string path;
};

class BHDSConnector_BRS : public Connector<PV01<Bond> >
{

public:

	// default ctor
	BHDSConnector_BRS(string path);

	// Publish data to the Connector
	void Publish(PV01<Bond> &data);
	void Publish(string _PersistId, PV01<Bond> &data);

private:
	string path;

};

class BHDSConnector_BES : public Connector<ExecutionOrder<Bond> >
{

public:

	// default ctor
	BHDSConnector_BES(string path);

	// Publish data to the Connector
	void Publish(ExecutionOrder<Bond> &data);
	void Publish(string _PersistId, ExecutionOrder<Bond> &data);

private:
	string path;
}; 

class BHDSConnector_BSS : public Connector<PriceStream<Bond> >
{

public:

	// default ctor
	BHDSConnector_BSS(string path);

	// Publish data to the Connector
	void Publish(PriceStream<Bond> &data);
	void Publish(string _PersistId, PriceStream<Bond> &data);

private:
	string path;
}; 

class BHDSConnector_BIS : public Connector<Inquiry<Bond> >
{

private: 
	string path;

public:

	// default ctor
	BHDSConnector_BIS(string path);

	// Publish data to the Connector
	void Publish(Inquiry<Bond> &data);
	void Publish(string _PersistId, Inquiry<Bond> &data);
};
#endif