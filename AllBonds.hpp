/*
AllBonds.hpp

This file is used to store bond information

Author: Corbin Guan
*/

#ifndef ALLBONDS_HPP
#define ALLBONDS_HPP

#include <map>
#include <string>
#include <vector>
#include "products.hpp"

map<string, Bond> all_bonds;
map<string, double> PV01_map;

void update_all_bond() {
	vector<string> CUSIPs{ "912828U99", "912828U73", "912828U81", "912828U57", "912828U24", "912810RU4" };
	Bond b1(CUSIPs[0], CUSIP, "T", 0.015, date(2018, 12, 17)); //2Y
	Bond b2(CUSIPs[1], CUSIP, "T", 0.025, date(2019, 12, 17)); //3Y
	Bond b3(CUSIPs[2], CUSIP, "T", 0.02, date(2021, 12, 17)); //5Y
	Bond b4(CUSIPs[3], CUSIP, "T", 0.05, date(2023, 12, 17)); //7Y
	Bond b5(CUSIPs[4], CUSIP, "T", 0.025, date(2026, 12, 17)); //10Y
	Bond b6(CUSIPs[5], CUSIP, "T", 0.3, date(2046, 12, 17)); // 30Y
	vector<Bond> all_bond_vec{b1, b2, b3, b4, b5, b6};

	for (int i = 0; i < 6; ++i)
	{
		all_bonds.insert(std::pair<string, Bond>(CUSIPs[i], all_bond_vec[i]));
	}
}

void update_PV01_map() {
	PV01_map.insert(std::pair<string, double>("912828U99", 2 * 0.8*99.832281*0.0001)); // 2Y
	PV01_map.insert(std::pair<string, double>("912828U73", 3 * 0.75*99.774758*0.0001)); // 3Y
	PV01_map.insert(std::pair<string, double>("912828U81", 5 * 0.7*99.952337*0.0001)); // 5Y
	PV01_map.insert(std::pair<string, double>("912828U57", 7 * 0.65*99.419378*0.0001)); // 7Y
	PV01_map.insert(std::pair<string, double>("912828U24", 10 * 0.6*95.758350*0.0001)); // 10Y
	PV01_map.insert(std::pair<string, double>("912810RU4", 30 * 0.55*94.656663*0.0001)); // 30Y
}


#endif