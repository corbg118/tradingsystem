/**
* BondListeners.hpp
* Defines the data types and Service for trade booking.
*
* @author Corbin Guan
*/
#ifndef BONDLISTENERS_HPP
#define BONDLISTENERS_HPP

#include <string>
#include <vector>
#include <map>
#include "soa.hpp"
#include "BondTradeBookingService.hpp"
#include "BondPositionService.hpp"
#include "BondRiskService.hpp"
#include "BondAlgoExecutionService.hpp"
#include "BondAlgoStreamingService.hpp"
#include "BondExecutionService.hpp"
#include "BondStreamingService.hpp"
#include "BondHistoricalDataService.hpp"

// Bond Trade Booking Service Listener, used to transfer data from BondBookingService to PositionService
class BTBSListener : public ServiceListener<Trade<Bond> > {
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BTBSListener(BondPositionService* BPS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(Trade<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(Trade<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(Trade<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondPositionService* BPS;
};

// BPS to BRS Listener
class BPSListener : public ServiceListener<Position<Bond> > {
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BPSListener(BondRiskService* BRS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(Position<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(Position<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(Position<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondRiskService* BRS;
};

// BMDS to BAES Listener
class BondMDSListener_BAES: public ServiceListener<OrderBook<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondMDSListener_BAES(BondAlgoExecutionService* BAES);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(OrderBook<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(OrderBook<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(OrderBook<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondAlgoExecutionService* BAES;
};

// BAES to BES Listener
class BondAESListener : public ServiceListener<ExecutionOrder<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondAESListener(BondExecutionService* BES);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(ExecutionOrder<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(ExecutionOrder<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(ExecutionOrder<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondExecutionService* BES;
};

// BPRS to BASS Listener
class BondPRSListener : public ServiceListener<Price<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondPRSListener(BondAlgoStreamingService* BASS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(Price<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(Price<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(Price<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondAlgoStreamingService* BASS;
};

// BASS to BSS Listener
class BondASSListener : public ServiceListener<PriceStream<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondASSListener(BondStreamingService* BSS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(PriceStream<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(PriceStream<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(PriceStream<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondStreamingService* BSS;
};

// BPS to BHDS Listener
class BondPSListener_BHDS : public ServiceListener<Position<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondPSListener_BHDS(BondHistoricalDataService_BPS* BHDS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(Position<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(Position<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(Position<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondHistoricalDataService_BPS* BHDS;
};


// BRS to BHDS Listener
class BondRSListener_BHDS : public ServiceListener<PV01<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondRSListener_BHDS(BondHistoricalDataService_BRS* BHDS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(PV01<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(PV01<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(PV01<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondHistoricalDataService_BRS* BHDS;
};

// BES to BHDS Listener
class BondESListener_BHDS : public ServiceListener<ExecutionOrder<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondESListener_BHDS(BondHistoricalDataService_BES* BHDS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(ExecutionOrder<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(ExecutionOrder<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(ExecutionOrder<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondHistoricalDataService_BES* BHDS;
};

// BSS to BHDS Listener
class BondSSListener_BHDS : public ServiceListener<PriceStream<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondSSListener_BHDS(BondHistoricalDataService_BSS* BHDS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(PriceStream<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(PriceStream<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(PriceStream<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondHistoricalDataService_BSS* BHDS;
};

// BIS to BHDS Listener
class BondISListener_BHDS : public ServiceListener<Inquiry<Bond> >
{
public:
	// This is important as we need to get the BTBSListener at the construction of service classes!
	BondISListener_BHDS(BondHistoricalDataService_BIS* BHDS);

	// Listener callback to process an add event to the Service --> used for TB and P
	virtual void ProcessAdd(Inquiry<Bond> &data) override;

	// Listener callback to process a remove event to the Service --> Not gonna be used here
	virtual void ProcessRemove(Inquiry<Bond> &data) override;

	// Listener callback to process an update event to the Service --> used for MD
	virtual void ProcessUpdate(Inquiry<Bond> &data) override;

private:
	// Construct a private member - BondPositionService Class
	BondHistoricalDataService_BIS* BHDS;
};

#endif