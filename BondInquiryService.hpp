/**
* BondInquiryService.hpp
* Defines the data types and Service for customer inquiries.
*
* @author Corbin Guan
*/
#ifndef BOND_INQUIRY_SERVICE_HPP
#define BOND_INQUIRY_SERVICE_HPP

#include "inquiryservice.hpp"
#include "products.hpp"
#include <unordered_map>

/**
* Service for customer inquirry objects.
* Keyed on inquiry identifier (NOTE: this is NOT a product identifier since each inquiry must be unique).
*/
class BondInquiryService : public InquiryService<Bond>
{

public:

	//default ctor
	BondInquiryService();

	// Get data on our service given a key, the key here MUST BE inquiry ID!
	Inquiry<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Inquiry<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<Inquiry<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<Inquiry<Bond> >* >& GetListeners() const;

	// Send a quote back to the client
	virtual void SendQuote(const string &inquiryId, double price);

	// Reject an inquiry from the client
	virtual void RejectInquiry(const string &inquiryId);

private:

	unordered_map<string, Inquiry<Bond> > inquiry_map; // Construct an unordered_map here as the inquiry is UNIQUE
	vector < ServiceListener<Inquiry<Bond> > * > listenerVec; // this vector is used to store the Listesners

};

#endif