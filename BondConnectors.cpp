/**
* BondConnectors.cpp
* Defines various BondConnectors
*
* @author Corbin Guan
*/
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include "BondConnectors.hpp"

using namespace std;

extern map<string, Bond> all_bonds;

// Define the Publish Function over here
void BTBSConnector::Publish(Trade<Bond> &data)
{
	// Not applicable
}

// Read data over when constructing the connector
BTBSConnector::BTBSConnector(string path, BondTradeBookingService* BTBS)
{
	// Read the files
	ifstream tradefile(path); // path = "trades.csv"
	// Define needed data members
	string line;
	string _CUSIP, _TradeID, _BookID, _Qty, _Side;

	// skip the first line
	getline(tradefile, line);

	int count = 0;
	// get the trade data and initilize a trade<Bond> data and save it into the BTBS class
	while (getline(tradefile, line)) {
		count++;
		cout << "The system is adding the " << count << "th trade data..." << endl;
		stringstream linedata(line);
		string bond_info;
		vector<string> data;
		while (getline(linedata, bond_info, '|')) data.push_back(bond_info);

		// Get CUSIP
		_CUSIP = data[0];

		// Constructe a new bond using the CUSIP ID
		Bond& b = all_bonds[_CUSIP];
		_TradeID = data[1];
		_BookID = data[2];
		// Get qty and convert it to long type
		_Qty = data[3];
		long qty = std::stol(_Qty);

		// Get side
		_Side = data[4];
		Side side = (_Side == "BUY" ? BUY : SELL);


		// Create an instance of trade
		Trade<Bond> trade_instance(b, _TradeID, _BookID, qty, side);
		string str = trade_instance.GetProduct().GetProductId();

		// Publish this instance
		BTBS->OnMessage(trade_instance);
	}
}

void BMDSConnector::Publish(OrderBook<Bond> &data)
{
	// Not applicable
}

// Read data over when constructing the connector
BMDSConnector::BMDSConnector(string path, BondMarketDataService* BMDS)
{
	ifstream is(path); // path = "marketdata.csv"
	string line;


	// skip the header
	getline(is, line);

	// to change i to be 1000000
	for (int i = 0; i < 10; i++) {
		cout << "The system is adding the " << i << "th market data..." << endl;
		getline(is, line);
		stringstream linedata(line);
		string order_book_info;
		vector<string> data;

		while (getline(linedata, order_book_info, '|')) data.push_back(order_book_info);
		string _CUSIP = data[0];

		// Define required variables
		PricingSide _SIDE;
		vector<Order> bid_stack, offer_stack;
		long _Qty;

		int index = 1; // use this index to get the orders, start from 1 as the CUSIP is at 0 position

					   // The first 10 data are my bid data
		for (int j = 0; j < 5; ++j) {
			// Convert price string to double
			string _priceStr = data[index++];
			size_t _priceIdx = _priceStr.find_first_of('-');
			double _Price = std::stoi(_priceStr.substr(0, _priceIdx));
			int num1 = std::stoi(_priceStr.substr(_priceIdx + 1, 2));
			char _priceCh = _priceStr[_priceStr.size() - 1];
			if (_priceCh == '+') _priceCh = '4';
			int num2 = _priceCh - '0';
			_Price += (num1 * 8 + num2) / 256.0;

			_Qty = std::stol(data[index++]);
			_SIDE = BID;
			Order bid_order(_Price, _Qty, _SIDE);
			bid_stack.push_back(bid_order);
		}

		// The next 10 data are my offer data
		for (int j = 0; j < 5; ++j) {
			// Convert price string to double
			string _priceStr = data[index++];
			size_t _priceIdx = _priceStr.find_first_of('-');
			double _Price = std::stoi(_priceStr.substr(0, _priceIdx));
			int num1 = std::stoi(_priceStr.substr(_priceIdx + 1, 2));
			char _priceCh = _priceStr[_priceStr.size() - 1];
			if (_priceCh == '+') _priceCh = '4';
			int num2 = _priceCh - '0';
			_Price += (num1 * 8 + num2) / 256.0;

			_Qty = std::stol(data[index++]);
			_SIDE = OFFER;
			Order offer_order(_Price, _Qty, _SIDE);
			offer_stack.push_back(offer_order);
		}
		Bond &b = all_bonds[_CUSIP];
		OrderBook<Bond> order_book(b, bid_stack, offer_stack);

		// Publish this instance
		BMDS->OnMessage(order_book);
	}
}

// Define the Publish Function over here
void BPRSConnector::Publish(Price<Bond> &data)
{
	// Not applicable 
}

// Read data over when constructing the connector
BPRSConnector::BPRSConnector(string path, BondPricingService* BPRS)
{
	ifstream pricefile(path); // path = "prices.csv"
	string line;

	// This is to skip the first line:
	getline(pricefile, line);

	// Define the elements
	string CUSIP, MidPriceStr, BidAskSpread;

	int count = 0;
	// get the trade data and initilize a trade<Bond> data and save it into the BTBS class
	while (getline(pricefile, line)) {
		count++;
		cout << "The system is adding the " << count << "th price data..." << endl;
		stringstream linedata(line);
		string price_info;
		vector<string> data;
		while (getline(linedata, price_info, '|')) data.push_back(price_info);

		CUSIP = data.at(0);
		MidPriceStr = data.at(1);
		BidAskSpread = data.at(2);

		// Match the bonds defined with the CUSIP ID above
		Bond b(CUSIP);

		// Convert the price string to price (mid)
		size_t _priceIdx = MidPriceStr.find_first_of('-');
		double _MidPrice = std::stoi(MidPriceStr.substr(0, _priceIdx));
		int num1 = std::stoi(MidPriceStr.substr(_priceIdx + 1, 2));
		char _priceCh = MidPriceStr[MidPriceStr.size() - 1];
		if (_priceCh == '+') _priceCh = '4';
		int num2 = _priceCh - '0';
		_MidPrice += (num1 * 8 + num2) / 256.0;

		// Convert the price string to price (bid-offer spread)
		_priceIdx = BidAskSpread.find_first_of('-');
		double _BidOfferSpread = std::stoi(BidAskSpread.substr(0, _priceIdx));
		num1 = std::stoi(BidAskSpread.substr(_priceIdx + 1, 2));
		_priceCh = BidAskSpread[BidAskSpread.size() - 1];
		if (_priceCh == '+') _priceCh = '4';
		num2 = _priceCh - '0';
		_BidOfferSpread += (num1 * 8 + num2) / 256.0;

		Price<Bond> price_instance(b, _MidPrice, _BidOfferSpread);

		// Publish this instance
		BPRS->OnMessage(price_instance);
	};
}

// default ctor
BESConnector::BESConnector(string path) : path(path) {
	ofstream os(path);
	os << "CUSIP|Side|OrderId|OrderType|Price|VisibleQty|HiddenQty|ParentOrderID|Child?" << endl;
}


// Publish data to the Connector
void BESConnector::Publish(ExecutionOrder<Bond> &data)
{
	ofstream BondExecution;
	BondExecution.open(path, ios::app); // path = "BondExecution.csv"
	string _side;
	string _ordertype = "Market";

	if (data.GetSide() == BID)  _side = "Bid";
	else _side = "Offer";


	BondExecution << data.GetProduct().GetProductId() << "|" << _side << "|" << data.GetOrderId() << "|" << _ordertype << "|" << data.GetPrice() << "|" << data.GetVisibleQuantity() <<
		"|" << data.GetHiddenQuantity() << "|" << data.GetParentOrderId() << "|" << data.IsChildOrder() << endl;
}

// default ctor
BSSConnector::BSSConnector(string path) : path(path) {
	ofstream os(path);
	os << "CUSIP|Side|Price|VisibleQty|HiddenQty|Side|Price|VisibleQty|HiddenQty" << endl;
}


// Publish data to the Connector
void BSSConnector::Publish(PriceStream<Bond> &data)
{
	ofstream BondExecution;
	BondExecution.open(path, ios::app); // path = "BondStreaming.csv"

	BondExecution << data.GetProduct().GetProductId() << "|" << "BID" << "|" << data.GetBidOrder().GetPrice() << "|" << data.GetBidOrder().GetVisibleQuantity() << "|" << data.GetBidOrder().GetHiddenQuantity()
		<< "|" << "OFFER" << "|" << data.GetOfferOrder().GetPrice() << "|" << data.GetOfferOrder().GetVisibleQuantity() << "|" << data.GetOfferOrder().GetHiddenQuantity() << endl;
}


// Define the Publish Function over here
void BISConnector::Publish(Inquiry<Bond> &data)
{
	// Not applicable 
}

// Read data over when constructing the connector
BISConnector::BISConnector(string path, BondInquiryService* BIS)
{
	ifstream inquiryfile(path); // path = "inquiries.csv"
	string line;

	// This is to skip the first line:
	getline(inquiryfile, line);

	// Define the elements
	string _InquiryId, _CUSIP, _SideStr, _State;
	long _Qty;
	//InquiryState State;

	int count = 0;
	// get the trade data and initilize a trade<Bond> data and save it into the BTBS class
	while (getline(inquiryfile, line)) {
		count++;
		cout << "The system is adding the " << count << "th inquiry data..." << endl;
		stringstream linedata(line);
		string inquiry_info;
		vector<string> data;
		while (getline(linedata, inquiry_info, '|')) data.push_back(inquiry_info);

		_InquiryId = data.at(0);
		_CUSIP = data.at(1);
		_SideStr = data.at(2);
		_Qty = std::stol(data.at(3));
		_State = data.at(4); // this is always "Received" over here
		Side _side;

		if (_SideStr == "BUY") _side = BUY;
		else _side = SELL;

		Bond b(_CUSIP);
		Inquiry<Bond> inquiry_instance(_InquiryId, b, _side, _Qty, 0, RECEIVED);

		// Publish this instance
		BIS->OnMessage(inquiry_instance);
	};
}

// default ctor
BHDSConnector_BPS::BHDSConnector_BPS(string path) : path(path) {
	// Add a listener to the BTBS at construction
	ofstream os(path);
	os << "PersistKey|CUSIP|BookID|Position|BookID|Position|BookID|Position|AggregatePosition" << endl;
}

// Publish data to the Connector
void BHDSConnector_BPS::Publish(Position<Bond> &data) {}

void BHDSConnector_BPS::Publish(string _persistId, Position<Bond> &data)
{
	ofstream BondHD;
	BondHD.open(path, ios::app); // path = "position.csv"
	string TRSY1 = "TRSY1", TRSY2 = "TRSY2 ", TRSY3 = "TRSY3";
	BondHD << _persistId << "|" << data.GetProduct().GetProductId() << "|TRSY1" << data.GetPosition(TRSY1)<< "|TRSY2" 
		<< data.GetPosition(TRSY2) << "|TRSY3" << data.GetPosition(TRSY3) << "|" << data.GetAggregatePosition() << endl;
}

// default ctor
BHDSConnector_BRS::BHDSConnector_BRS(string path) : path(path) {
	ofstream os(path);
	os << "PersistKey|CUSIP|PV01|AggregatePV01" << endl;
}


// Publish data to the Connector
void BHDSConnector_BRS::Publish(PV01<Bond> &data) {}

void BHDSConnector_BRS::Publish(string _persistId, PV01<Bond> &data)
{
	ofstream BondHD;
	BondHD.open(path, ios::app); // path = "risk.csv"
	BondHD << _persistId << "|" << data.GetProducts().at(0).GetProductId() << "|" << data.GetPV01() << endl;
}

// default ctor
BHDSConnector_BES::BHDSConnector_BES(string path) : path(path){
	ofstream os(path);
	os << "PersistId|CUSIP|Side|OrderId|OrderType|Price|VisibleQty|HiddenQty|ParentOrderID|Child?" << endl;
}


// Publish data to the Connector
void BHDSConnector_BES::Publish(ExecutionOrder<Bond> &data) {}

void BHDSConnector_BES::Publish(string _persistId, ExecutionOrder<Bond> &data)
{
	ofstream BondHD;
	BondHD.open(path, ios::app); // path = "executions.csv"
	string _side;
	string _ordertype = "Market";

	if (data.GetSide() == BID)  _side = "Bid";
	else _side = "Offer";

	BondHD << _persistId << "|" << data.GetProduct().GetProductId() << "|" << _side << "|" << data.GetOrderId() << "|" << _ordertype << "|" << data.GetPrice() << "|" << data.GetVisibleQuantity() <<
		"|" << data.GetHiddenQuantity() << "|" << data.GetParentOrderId() << "|" << data.IsChildOrder() << endl;
}

// default ctor
BHDSConnector_BSS::BHDSConnector_BSS(string path) : path(path) {
	ofstream os(path);
	os << "PersistId|CUSIP|Side|Price|VisibleQty|HiddenQty|Side|Price|VisibleQty|HiddenQty" << endl;
}


// Publish data to the Connector
void BHDSConnector_BSS::Publish(PriceStream<Bond> &data) {}

void BHDSConnector_BSS::Publish(string _persistId, PriceStream<Bond> &data)
{
	ofstream BondHD;
	BondHD.open(path, ios::app); // path = "streaming.csv"
	BondHD << _persistId << "|" << data.GetProduct().GetProductId() << "|" << "BID" << "|" << data.GetBidOrder().GetPrice() << "|" 
		<< data.GetBidOrder().GetVisibleQuantity() << "|" << data.GetBidOrder().GetHiddenQuantity()
		<< "|" << "OFFER" << "|" << data.GetOfferOrder().GetPrice() << "|" << data.GetOfferOrder().GetVisibleQuantity() << "|" 
		<< data.GetOfferOrder().GetHiddenQuantity() << endl;
}

// default ctor
BHDSConnector_BIS::BHDSConnector_BIS(string path):path(path) {
	ofstream os(path);
	os << "PersistId|InquiryId|CUSIP|Status" << endl;

}

// Publish data to the Connector
void BHDSConnector_BIS::Publish(Inquiry<Bond> &data) {}

void BHDSConnector_BIS::Publish(string _persistId, Inquiry<Bond> &data)
{
	ofstream BondHD;
	BondHD.open(path, ios::app); // path = "allinquiries.csv"
	string _state;
	if (data.GetState() == QUOTED) _state = "QUOTED";
	else if (data.GetState() == REJECTED) _state = "REJECTED";
	else if (data.GetState() == RECEIVED) _state = "RECEIVED";
	else _state = "OTHER";

	BondHD << _persistId << "|" << data.GetInquiryId() << "|" << data.GetProduct().GetProductId() << "|" << _state << endl;
}

