#ifndef BONDEXECUTIONSERVICE_HPP
#define BONDEXECUTIONSERVICE_HPP

#include "executionservice.hpp"
#include "BondConnectors.hpp"
#include "products.hpp"

/**
* Service for executing orders on an exchange.
* Keyed on product identifier.
* Type T is the product type.
*/

class BondExecutionService : public ExecutionService<Bond>
{

public:

	//default ctor
	BondExecutionService(BESConnector* BESC);

	// Execute an order on a market
	void ExecuteOrder(ExecutionOrder<Bond>& order, Market market);
		
	// Get data on our service given a key
	ExecutionOrder<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(ExecutionOrder<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<ExecutionOrder<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<ExecutionOrder<Bond> >* >& GetListeners() const;

private:
	multimap<string, ExecutionOrder<Bond> > ExecutionMap;
	vector<ServiceListener<ExecutionOrder<Bond> >*> listenerVec;
	BESConnector* BESC;
};

#endif