/**
* bondtradebookingservice.cpp
* Defines the data types and Service for trade booking.
*
* @author Corbin Guan
*/

#include "BondTradeBookingService.hpp"
#include "BondListeners.hpp"

// Default ctor
BondTradeBookingService::BondTradeBookingService() : TradeBookingService() {}

void BondTradeBookingService::BookTrade(Trade<Bond> &trade) {
	// book trades into the trade map
	tr.insert(std::pair<string, Trade<Bond> >(trade.GetTradeId(), trade));
	//cout << "TradeId: " << tr.at(trade.GetTradeId()).GetTradeId() << " size: "<<tr.at(trade.GetTradeId()).GetQuantity()<<endl;

	// at the same time we call the BTBSListener to pass the trade to PositionService
	for (auto listener : listenerVec)
		listener->ProcessAdd(trade);
}

Trade<Bond>& BondTradeBookingService::GetData(string key) {
	return tr[key]; // return the trade corresponding to the key
}

void BondTradeBookingService::OnMessage(Trade<Bond> &data) {
	// this is to be invoked by the the Connector to flow files inside
	cout << "The system is now booking the trades..." << endl;
	BookTrade(data);
}

void BondTradeBookingService::AddListener(ServiceListener<Trade<Bond> > *listener) {
	listenerVec.push_back(listener);
}

const vector< ServiceListener<Trade<Bond> >* >& BondTradeBookingService::GetListeners() const {
	return listenerVec;
}