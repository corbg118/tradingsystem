/**
* bondhistoricaldataservice.hpp
* Defines the data types and Service for Bond historical data.
*
* @author Corbin Guan
*/
#include "BondHistoricalDataService.hpp"
#include "BondConnectors.hpp"
#include "positionservice.hpp"
/**
* Service for processing and persisting historical data to a persistent store.
* Keyed on some persistent key.
*/

BondHistoricalDataService_BPS::BondHistoricalDataService_BPS(BHDSConnector_BPS* BHDSC):BHDSC(BHDSC) {}

// Persist data to a store
void BondHistoricalDataService_BPS::PersistData(string persistKey, Position<Bond>& data)
{
	// This function shall be linked to the listeners
	history_map.insert(std::pair<string, Position<Bond> >(persistKey, data));
	// Incur the connector to publish the data
	// Push data to the connector to output the datas
	BHDSC->Publish(persistKey, data);
}

// Get data on our service given a key
Position<Bond>& BondHistoricalDataService_BPS::GetData(string key)
{
	// return this data item related to the key
	return history_map[key];
}

// The callback that a Connector should invoke for any new or updated data
void BondHistoricalDataService_BPS::OnMessage(Position<Bond> &data)
{
	// This function shall be linked to the connectors
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondHistoricalDataService_BPS::AddListener(ServiceListener<Position<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<Position<Bond> >* >& BondHistoricalDataService_BPS::GetListeners() const
{
	return listenerVec;
}

BondHistoricalDataService_BRS::BondHistoricalDataService_BRS(BHDSConnector_BRS* BHDSC) : BHDSC(BHDSC) {}

// Persist data to a store
void BondHistoricalDataService_BRS::PersistData(string persistKey, PV01<Bond>& data)
{
	// This function shall be linked to the listeners
	history_map.insert(std::pair<string, PV01<Bond> >(persistKey, data));
	// Incur the connector to publish the data
	// Push data to the connector to output the datas
	BHDSC->Publish(persistKey, data);
}

// Get data on our service given a key
PV01<Bond>& BondHistoricalDataService_BRS::GetData(string key)
{
	// return this data item related to the key
	return history_map[key];
}

// The callback that a Connector should invoke for any new or updated data
void BondHistoricalDataService_BRS::OnMessage(PV01<Bond> &data)
{
	// This function shall be linked to the connectors
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondHistoricalDataService_BRS::AddListener(ServiceListener<PV01<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<PV01<Bond> >* >& BondHistoricalDataService_BRS::GetListeners() const
{
	return listenerVec;
}

BondHistoricalDataService_BES::BondHistoricalDataService_BES(BHDSConnector_BES* BHDSC) : BHDSC(BHDSC){}

// Persist data to a store
void BondHistoricalDataService_BES::PersistData(string persistKey, ExecutionOrder<Bond>& data)
{
	// This function shall be linked to the listeners
	history_map.insert(std::pair<string, ExecutionOrder<Bond> >(persistKey, data));
	// Incur the connector to publish the data
	// Push data to the connector to output the datas
	BHDSC->Publish(persistKey, data);
}

// Get data on our service given a key
ExecutionOrder<Bond>& BondHistoricalDataService_BES::GetData(string key)
{
	// return this data item related to the key
	return history_map[key];
}

// The callback that a Connector should invoke for any new or updated data
void BondHistoricalDataService_BES::OnMessage(ExecutionOrder<Bond> &data)
{
	// This function shall be linked to the connectors
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondHistoricalDataService_BES::AddListener(ServiceListener<ExecutionOrder<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<ExecutionOrder<Bond> >* >& BondHistoricalDataService_BES::GetListeners() const
{
	return listenerVec;
}

BondHistoricalDataService_BSS::BondHistoricalDataService_BSS(BHDSConnector_BSS* BHDSC) : BHDSC (BHDSC) {}

// Persist data to a store
void BondHistoricalDataService_BSS::PersistData(string persistKey, PriceStream<Bond>& data)
{
	// This function shall be linked to the listeners
	history_map.insert(std::pair<string, PriceStream<Bond> >(persistKey, data));
	// Incur the connector to publish the data
	// Push data to the connector to output the datas
	BHDSC->Publish(persistKey, data);
}

// Get data on our service given a key
PriceStream<Bond>& BondHistoricalDataService_BSS::GetData(string key)
{
	// return this data item related to the key
	return history_map[key];
}

// The callback that a Connector should invoke for any new or updated data
void BondHistoricalDataService_BSS::OnMessage(PriceStream<Bond> &data)
{
	// This function shall be linked to the connectors
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondHistoricalDataService_BSS::AddListener(ServiceListener<PriceStream<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<PriceStream<Bond> >* >& BondHistoricalDataService_BSS::GetListeners() const
{
	return listenerVec;
}

// ctor
BondHistoricalDataService_BIS::BondHistoricalDataService_BIS(BHDSConnector_BIS* _BHDSC) : BHDSC(_BHDSC) {}

// Persist data to a store
void BondHistoricalDataService_BIS::PersistData(string persistKey, Inquiry<Bond>& data)
{
	// This function shall be linked to the listeners
	history_map.insert(std::pair<string, Inquiry<Bond> >(persistKey, data));
	// Incur the connector to publish the data
	// Push data to the connector to output the datas
	BHDSC->Publish(persistKey, data);
}

// Get data on our service given a key
Inquiry<Bond>& BondHistoricalDataService_BIS::GetData(string key)
{
	// return this data item related to the key
	return history_map[key];
}

// The callback that a Connector should invoke for any new or updated data
void BondHistoricalDataService_BIS::OnMessage(Inquiry<Bond> &data)
{
	// This function shall be linked to the connectors
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondHistoricalDataService_BIS::AddListener(ServiceListener<Inquiry<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<Inquiry<Bond> >* >& BondHistoricalDataService_BIS::GetListeners() const
{
	return listenerVec;
}