/**
* BondRiskService.hpp
* Defines the data types and Service for trade booking.
*
* @author Corbin Guan
*/
#ifndef BONDRISKSERVICE_HPP
#define BONDRISKSERVICE_HPP

#include <string>
#include <vector>
#include <map>
#include "riskservice.hpp"
#include "products.hpp"

/**
* Risk Service to vend out risk for a particular security and across a risk bucketed sector.
* Keyed on product identifier.
* Type T is the product type.
*/

class BondRiskService : public RiskService<Bond>
{
private:
	vector < ServiceListener<PV01<Bond> > * > listenerVec; // this vector is used to store the Listesners
	map<string, PV01<Bond> > risk;

public:

	// default ctor
	BondRiskService();

	// Add a position that the service will risk
	void AddPosition(Position<Bond> &position);

	// Get the bucketed risk for the bucket sector
	const PV01<Bond> GetBucketedRisk(const BucketedSector<Bond> &sector) const;

	// Get data on our service given a key
	PV01<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(PV01<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<PV01<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<PV01<Bond> >* >& GetListeners() const;

};

#endif