/**
* bondtradebookingservice.hpp
* Defines the data types and Service for trade booking.
*
* @author Corbin Guan
*/
#ifndef BONDTRADEBOOKINGSERVICE_HPP
#define BONDTRADEBOOKINGSERVICE_HPP

#include <string>
#include <vector>
#include <map>
#include "tradebookingservice.hpp"
#include "products.hpp"

/**
* Bond Trade Booking Service to book bond trades to a particular book.
*/
class BondTradeBookingService : public TradeBookingService<Bond>
{
protected:
	vector < ServiceListener<Trade<Bond> > * > listenerVec; // this vector is used to store the Listesners
	map<string, Trade<Bond> > tr;

public:
	// Default ctor
	BondTradeBookingService();

	// Book the trade
	void BookTrade(Trade<Bond> &trade);
	// Get data on our service given a key
	Trade<Bond>& GetData(string key);
	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(Trade<Bond> &data);
	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<Trade<Bond> > *listener);
	// Get all listeners on the Service.
	const vector< ServiceListener<Trade<Bond> >* >& GetListeners() const;
};

#endif