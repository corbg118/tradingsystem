#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <random>
#include "BondConnectors.hpp"
#include "BondListeners.hpp"
#include "TxtGenerators.hpp"
#include "AllBonds.hpp"
using namespace std;

int main()	
{
	// The following two functions are used to initialize the bond map and PV01 map
	update_all_bond();
	update_PV01_map();
	
	// The following functions are called to generate data, they can be excluded once the files are generated
	// Trade_Generator();
	// MD_Generator();
	// Price_Generator();
	// Inquiries_Generator();

	// Initialize and call the BondTradeBookingService Connector to file in data from .txt file
	cout << "Running: trades.csv -> BondTradeBookingServiceConnector -> BondTradeBookingService -> BondTBSListener -> BondPositionService -> BPSListener -> BondRiskService -> BRSListener -> BondHistoricalDataService -> BHDSConnector -> risk.csv" << endl;
	BondRiskService BRS;
	BHDSConnector_BRS BHDSC_risk("risk.csv"); // output from Hitorical Data 
	BondHistoricalDataService_BRS BHDS_risk(&BHDSC_risk);
	BondRSListener_BHDS BRS_BHDS_listener(&BHDS_risk);
	BRS.AddListener(&BRS_BHDS_listener);

	BHDSConnector_BPS BHDSC_position("position.csv"); // output from Hitorical Data 
	BondHistoricalDataService_BPS BHDS_position(&BHDSC_position);
	BondPSListener_BHDS BPS_BHDS_listener(&BHDS_position);

	BondPositionService BPS;
	BPSListener BPS_BRS_listener(&BRS);
	BPS.AddListener(&BPS_BHDS_listener);
	BPS.AddListener(&BPS_BRS_listener);

	BTBSListener BTBS_BPS_listener(&BPS);
	BondTradeBookingService BTBS;
	BTBS.AddListener(&BTBS_BPS_listener);

	// Get trades data
	BTBSConnector BTBSC("trades.csv", &BTBS);


	// Initialize and call the BondMarketDataService Connector to file in data from .txt file
	cout << "Running: prices.csv->BondPricingServiceConnector->BondPricingService->BondPRSListener->BondAlgoStreamingService->BASSListener->BondStreamingService->BSSConnector->BondStreaming.csv" << endl;
	cout << "Running: BondStreamingService -> BSSListener -> BondHistoricalDataService -> BHDSConnector -> streaming.csv" << endl;
	BSSConnector BSSC("BondStreaming.csv"); // output from BSS directly
	BondStreamingService BSS(&BSSC);
	BHDSConnector_BSS BHDSC_streaming("streaming.csv"); // output from Hitorical Data 
	BondHistoricalDataService_BSS BHDS_streaming(&BHDSC_streaming);
	BondSSListener_BHDS BSS_BHDS_listener(&BHDS_streaming);
	BSS.AddListener(&BSS_BHDS_listener);

	BondAlgoStreamingService BASS;
	BondASSListener BASS_BSS_listener(&BSS);
	BASS.AddListener(&BASS_BSS_listener);

	BondPRSListener BPRS_BASS_listener(&BASS);
	BondPricingService BPRS;
	BPRS.AddListener(&BPRS_BASS_listener);

	// Get prices data
	BPRSConnector BPRSC("prices.csv", &BPRS);


	// Initialize and call the BondPricingService Connector to file in data from .txt file
	// Initialize the connections for bond market data services
	cout << "Running: marketdata.csv->BondMarketDataServiceConnector->BondMarketDataService->BondMDSListener->BondAlgoExecutionService->BAESListener->BondExecutionService->BESConnector->BondExecution.csv" << endl;
	cout << "Running: BondExecutionService -> BESListener -> BondHistoricalDataService -> BHDSConnector -> executions.csv" << endl;
	BESConnector BESC("BondExecution.csv"); // output from BES directly
	BondExecutionService BES(&BESC);
	BHDSConnector_BES BHDSC_execution("executions.csv"); // output from Hitorical Data 
	BondHistoricalDataService_BES BHDS_execution(&BHDSC_execution);
	BondESListener_BHDS BES_BHDS_listener(&BHDS_execution);
	BES.AddListener(&BES_BHDS_listener);

	BondAlgoExecutionService BAES;
	BondAESListener BAES_BES_listener(&BES);
	BAES.AddListener(&BAES_BES_listener);

	BondMDSListener_BAES BMDS_BAES_listener(&BAES);
	BondMarketDataService BMDS;
	BMDS.AddListener(&BMDS_BAES_listener);

	// Get market data
	BMDSConnector BMDSC("marketdata.csv", &BMDS);


	// Initialize and call the BondInquiryService Connector to file in data from .txt file
	// Initialize the connections for bond inquiry services
	cout << "Running: inquiries.csv -> BondInquiryServiceConnector -> BondInquiryService -> BISListener -> BondHistoricalDataService -> BHDSConnector -> allinquiries.csv" << endl;
	BondInquiryService BIS;
	BHDSConnector_BIS BHDSC_inquiry("allinquiries.csv"); // bond historical data service connector -> inquiry
	BondHistoricalDataService_BIS BHDS_inquiry(&BHDSC_inquiry);
	BondISListener_BHDS BIS_BHDS_listener (&BHDS_inquiry);

	BIS.AddListener(&BIS_BHDS_listener); // HDS listens to IS

	// Get inquiries
	BISConnector BISC("inquiries.csv", &BIS);


	return 0;
}