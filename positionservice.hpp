/**
 * positionservice.hpp
 * Defines the data types and Service for positions.
 *
 * @author Breman Thuraisingham
 */
#ifndef POSITION_SERVICE_HPP
#define POSITION_SERVICE_HPP

#include <string>
#include <map>
#include "soa.hpp"
#include "tradebookingservice.hpp"

using namespace std;

/**
 * Position class in a particular book.
 * Type T is the product type.
 */
template<typename T>
class Position
{

public:
	// default ctor
	Position();

  // ctor for a position
  Position(const T &_product);
  
  // Update position
  void UpdatePosition(string BookId, long PositionSize);

  // Get the product
  const T& GetProduct() const;

  // Get the position quantity
  long GetPosition(string &book);

  // Get the aggregate position
  long GetAggregatePosition();

private:
  T product;
  map<string,long> positions;

};

/**
 * Position Service to manage positions across multiple books and secruties.
 * Keyed on product identifier.
 * Type T is the product type.
 */
template<typename T>
class PositionService : public Service<string,Position <T> >
{
public:

  // Add a trade to the service
  virtual void AddTrade(Trade<T> &trade) = 0;

};

template<typename T>
Position<T>::Position() {}


template<typename T>
Position<T>::Position(const T &_product) :
  product(_product)
{
}

template<typename T>
const T& Position<T>::GetProduct() const
{
  return product;
}

template<typename T>
void Position<T>::UpdatePosition(string book, long positionsize)
{
	// Update the new position to the book, we can add directly here
	positions[book] += positionsize;
}


template<typename T>
long Position<T>::GetPosition(string &book)
{
  return positions[book];
}

template<typename T>
long Position<T>::GetAggregatePosition()
{
	// No-op implementation - should be filled out for implementations
	// This returns the aggregate position of the product
	return (positions["TRSY1"] + positions["TRSY2"] + positions["TRSY3"]);
}

#endif
