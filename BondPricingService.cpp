/**
* BondPostionService.cpp
* Defines the data types and Service for position.
*
* @author Corbin Guan
*/

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include "BondPricingService.hpp"
#include "BondListeners.hpp"

BondPricingService::BondPricingService() : PricingService() {}

// Add price
void BondPricingService::AddPrice(Price<Bond> &price) {
	// book price into the price map
	price_map.insert(std::pair<string, Price<Bond> >(price.GetProduct().GetProductId(), price));

	cout << "CUSIP: " << price.GetProduct().GetProductId() << " mid: " << price.GetMid() << " BASpread: " << price.GetBidOfferSpread() << endl;
	// at the same time we call the BTBSListener to pass the trade to PositionService
	for (auto listener : listenerVec)
		listener->ProcessAdd(price);
}


// Get data on our service given a key
Price<Bond>& BondPricingService::GetData(string key) {
	// Return the first instance of the price
	auto it = price_map.lower_bound(key);
	return it->second;
}

// The callback that a Connector should invoke for any new or updated data
void BondPricingService::OnMessage(Price<Bond> &data)
{
	// Add info directly to the BPRS, we need to add the price to the price_map
	cout << "The system is reading the pricing data..." << endl;
	AddPrice(data);
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondPricingService::AddListener(ServiceListener<Price<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<Price<Bond> >* >& BondPricingService::GetListeners() const
{
	return listenerVec;
}

