#ifndef BONDSTREAMINGSERVICE_HPP
#define BONDSTREAMINGSERVICE_HPP

#include "streamingservice.hpp"
#include "BondConnectors.hpp"
#include "products.hpp"

/**
* Streaming service to publish two-way prices.
* Keyed on product identifier.
* Type T is the product type.
*/
class BondStreamingService : public StreamingService<Bond>
{

public:

	//default ctor
	BondStreamingService(BSSConnector* BSSC);

	// Publish two-way prices
	void PublishPrice(PriceStream<Bond>& priceStream);

	// Get data on our service given a key
	PriceStream<Bond>& GetData(string key);

	// The callback that a Connector should invoke for any new or updated data
	void OnMessage(PriceStream<Bond> &data);

	// Add a listener to the Service for callbacks on add, remove, and update events
	// for data to the Service.
	void AddListener(ServiceListener<PriceStream<Bond> > *listener);

	// Get all listeners on the Service.
	const vector< ServiceListener<PriceStream<Bond> >* >& GetListeners() const;

private:
	multimap<string, PriceStream<Bond> > StreamingMap;
	vector<ServiceListener<PriceStream<Bond> >* > listenerVec;
	BSSConnector* BSSC;
};

#endif