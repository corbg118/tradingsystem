/**
* BondPostionService.cpp
* Defines the data types and Service for position.
*
* @author Corbin Guan
*/

#include "BondMarketDataService.hpp"
#include "BondListeners.hpp"


BondMarketDataService::BondMarketDataService() : MarketDataService<Bond>() {}

void BondMarketDataService::AddOrder(OrderBook<Bond> &order)
{
	// book order into the order map
	order_map.insert(std::pair<string, OrderBook<Bond> >(order.GetProduct().GetProductId(), order));
	// at the same time we call the BMDSListeners to pass the order to other services
	for (auto listener : listenerVec)
		listener->ProcessAdd(order);
}

// Get the best bid/offer order
// Find the largest bid price, and smallest offer price
const BidOffer BondMarketDataService::GetBestBidOffer(const string &productId)
{
	Order _bidTmp(0, 0, BID), _offerTmp(100000, 0, OFFER);
	// To iterate all elements of the multimap with same productID to find the best bid/offer
	auto it = order_map.lower_bound(productId);
	auto it2 = order_map.upper_bound(productId);

	while (it != it2)
	{
		// The key here is to make sure the best price for bid/offer is always at postion 1!!!
		if (it->second.GetBidStack().at(0).GetPrice() > _bidTmp.GetPrice()) _bidTmp = it->second.GetBidStack().at(0);
		if (it->second.GetOfferStack().at(0).GetPrice() < _offerTmp.GetPrice()) _offerTmp = it->second.GetOfferStack().at(0);
	}

//	BidOffer ret(_bidTmp, _offerTmp);
	return BidOffer(_bidTmp, _offerTmp);
}

// Aggregate the order book
const OrderBook<Bond>& BondMarketDataService::AggregateDepth(const string &productId)
{
	// This is to aggregate all positions of the order book, since our data were constructed in a way that each level of order is
	// differnt. Therefore, the following definition works as a demo only.

	// find any order book of the product:
	auto it = order_map.lower_bound(productId);
	return it->second;
	//vector<Order> _BidStack = it->second.GetBidStack();
	//vector<Order> _AskStack = it->second.GetOfferStack();

	//for (auto it2 = _BidStack.begin(); it2 != _BidStack.end(); it2++)
	//{
	//	if (it2 == _BidStack.begin()) continue;
	//	if (it2->GetPrice() == (it2-1)->GetPrice()) 
	//}

}

// Get data on our service given a key
OrderBook<Bond>& BondMarketDataService::GetData(string key) {
	// Return the latest orderbook encountered
	auto it = order_map.upper_bound(key);
	return it->second;
}

// The callback that a Connector should invoke for any new or updated data
void BondMarketDataService::OnMessage(OrderBook<Bond> &data)
{
	// Add info directly to the BMDS, after obtaining the orderbook, we need to add the orderbook to the order_map
	cout << "The system is booking orders into the Bond Market Data Service..." << endl;
	AddOrder(data);
}

// Add a listener to the Service for callbacks on add, remove, and update events
// for data to the Service.
void BondMarketDataService::AddListener(ServiceListener<OrderBook<Bond> > *listener)
{
	listenerVec.push_back(listener);
}

// Get all listeners on the Service.
const vector< ServiceListener<OrderBook<Bond> >* >& BondMarketDataService::GetListeners() const
{
	return listenerVec;
}